Seq-entry ::= set {
  level 1 ,
  class nuc-prot ,
  descr {
    source {
      org {
        taxname "Ovis aries" ,
        common "sheep" ,
        db {
          {
            db "taxon" ,
            tag
              id 9940 } } ,
        orgname {
          name
            binomial {
              genus "Ovis" ,
              species "aries" } ,
          mod {
            {
              subtype strain ,
              subname "domestic" } } ,
          lineage "Eukaryota; Metazoa; Chordata; Craniata; Vertebrata;
 Euteleostomi; Mammalia; Eutheria; Laurasiatheria; Cetartiodactyla;
 Ruminantia; Pecora; Bovidae; Caprinae; Ovis" ,
          gcode 1 ,
          mgcode 2 ,
          div "MAM" } } ,
      subtype {
        {
          subtype other ,
          name "Allele: C4-2H.1" } } } ,
    pub {
      pub {
        sub {
          authors {
            names
              std {
                {
                  name
                    name {
                      last "Ren" ,
                      initials "X." } } } ,
            affil
              str "X. Ren, The MRC Immunochemistry Unit, Dept of Biochemistry,
 University of Oxford, South Parks Road, Oxford OX1 3QU, UK" } ,
          medium other ,
          date
            std {
              year 1992 ,
              month 6 ,
              day 22 } } } } ,
    pub {
      pub {
        pmid 8423050 ,
        article {
          title {
            name "The thioester and isotypic sites of complement component C4
 in sheep and cattle." } ,
          authors {
            names
              std {
                {
                  name
                    name {
                      last "Ren" ,
                      initials "X.D." } } ,
                {
                  name
                    name {
                      last "Dodds" ,
                      initials "A.W." } } ,
                {
                  name
                    name {
                      last "Law" ,
                      initials "S.K." } } } ,
            affil
              str "Department of Biochemistry, University of Oxford, UK." } ,
          from
            journal {
              title {
                iso-jta "Immunogenetics" ,
                ml-jta "Immunogenetics" ,
                issn "0093-7711" ,
                name "Immunogenetics" } ,
              imp {
                date
                  std {
                    year 1993 } ,
                volume "37" ,
                issue "2" ,
                pages "120-128" ,
                language "eng" ,
                pubstatus ppublish ,
                history {
                  {
                    pubstatus pubmed ,
                    date
                      std {
                        year 1993 ,
                        month 1 ,
                        day 1 } } ,
                  {
                    pubstatus medline ,
                    date
                      std {
                        year 1993 ,
                        month 1 ,
                        day 1 ,
                        hour 0 ,
                        minute 1 } } ,
                  {
                    pubstatus other ,
                    date
                      std {
                        year 1993 ,
                        month 1 ,
                        day 1 ,
                        hour 0 ,
                        minute 0 } } } } } ,
          ids {
            doi "10.1007/BF00216835" ,
            pubmed 8423050 } } } } ,
    comment "See also X66994-99" ,
    update-date
      std {
        year 2016 ,
        month 7 ,
        day 14 } } ,
  seq-set {
    seq {
      id {
        embl {
          accession "X66994" ,
          version 1 } ,
        gi 1234 } ,
      descr {
        title "O.aries (C4-2H.1) C4 gene" ,
        molinfo {
          biomol genomic } ,
        embl {
          div mam ,
          creation-date
            std {
              year 1992 ,
              month 7 ,
              day 10 } ,
          update-date
            std {
              year 2006 ,
              month 11 ,
              day 14 } ,
          keywords {
            "complement component" ,
            "complement component C4" } } ,
        create-date
          std {
            year 1992 ,
            month 7 ,
            day 10 } } ,
      inst {
        repr raw ,
        mol dna ,
        length 945 ,
        seq-data
          ncbi2na '50A79AA040538577E9D411E9E7D59C5E8421624BA25E7955E214285996E
8DD35202BDE2E49A624A2EA94A22A4BC3A09EA497ABAD224A8222392D54BA00AAF17FD45ECEBB8
5D2793F5FD429C4668D4A0FD80088EBD73AA7EBC4DA84B245EB8BFA28BB5EAEEEA2A94AFEB8492
25273CEA2240CA1592CFAD53B91ECBB7229CD509CE5FDDD7729D1A5FEE78231E2FE9528D2BA9A7
71C009C4A21253BA79FD5241A8E3A74F51855ED7BCD44A8CE4ACE89EA29EA52A9128AA22D15E55
D9476EBE79E79E70B6F12DBB5077B9855321A49514A759ED5EABDD4A42047A2EAF94FD7DD43939
0BBFDE547D052A29F2EA098C0'H } ,
      annot {
        {
          data
            ftable {
              {
                data
                  rna {
                    type mRNA } ,
                partial TRUE ,
                location
                  mix {
                    int {
                      from 0 ,
                      to 133 ,
                      id
                        gi 1234 ,
                      fuzz-from
                        lim lt } ,
                    int {
                      from 298 ,
                      to 373 ,
                      id
                        gi 1234 } ,
                    int {
                      from 530 ,
                      to 686 ,
                      id
                        gi 1234 } ,
                    int {
                      from 924 ,
                      to 944 ,
                      id
                        gi 1234 ,
                      fuzz-to
                        lim gt } } } ,
              {
                data
                  imp {
                    key "exon" } ,
                partial TRUE ,
                location
                  int {
                    from 0 ,
                    to 133 ,
                    id
                      gi 1234 ,
                    fuzz-from
                      lim lt } ,
                qual {
                  {
                    qual "number" ,
                    val "24" } } } ,
              {
                data
                  imp {
                    key "misc_feature" } ,
                comment "thioester site" ,
                location
                  int {
                    from 7 ,
                    to 18 ,
                    id
                      gi 1234 } } ,
              {
                data
                  imp {
                    key "intron" } ,
                location
                  int {
                    from 134 ,
                    to 297 ,
                    id
                      gi 1234 } ,
                qual {
                  {
                    qual "number" ,
                    val "24" } } } ,
              {
                data
                  imp {
                    key "exon" } ,
                location
                  int {
                    from 298 ,
                    to 373 ,
                    id
                      gi 1234 } ,
                qual {
                  {
                    qual "number" ,
                    val "25" } } } ,
              {
                data
                  imp {
                    key "intron" } ,
                location
                  int {
                    from 374 ,
                    to 529 ,
                    id
                      gi 1234 } ,
                qual {
                  {
                    qual "number" ,
                    val "25" } } } ,
              {
                data
                  imp {
                    key "exon" } ,
                location
                  int {
                    from 530 ,
                    to 686 ,
                    id
                      gi 1234 } ,
                qual {
                  {
                    qual "number" ,
                    val "26" } } } ,
              {
                data
                  imp {
                    key "misc_feature" } ,
                comment "isotypic site" ,
                location
                  int {
                    from 657 ,
                    to 674 ,
                    id
                      gi 1234 } } ,
              {
                data
                  imp {
                    key "intron" } ,
                location
                  int {
                    from 687 ,
                    to 923 ,
                    id
                      gi 1234 } ,
                qual {
                  {
                    qual "number" ,
                    val "26" } } } ,
              {
                data
                  imp {
                    key "exon" } ,
                partial TRUE ,
                location
                  int {
                    from 924 ,
                    to 944 ,
                    id
                      gi 1234 ,
                    fuzz-to
                      lim gt } ,
                qual {
                  {
                    qual "number" ,
                    val "27" } } } ,
              {
                data
                  gene {
                    locus "C4" } ,
                partial TRUE ,
                location
                  int {
                    from 0 ,
                    to 944 ,
                    id
                      gi 1234 ,
                    fuzz-from
                      lim lt ,
                    fuzz-to
                      lim gt } } } } } } ,
    seq {
      id {
        embl {
          accession "CAA47393" ,
          version 1 } ,
        gi 1235 } ,
      descr {
        title "complement component C4, partial [Ovis aries]" ,
        molinfo {
          biomol peptide ,
          completeness no-ends } ,
        create-date
          std {
            year 1992 ,
            month 7 ,
            day 10 } } ,
      inst {
        repr raw ,
        mol aa ,
        length 129 ,
        seq-data
          ncbieaa "QGCGEQTMTLLAPTLAASRYLDKTEQWSLLPPETKDRAVDLIQKGYTRIQEFRKRDGSY
GAWLHRDSSTWLTAFVLKILSLAQDQVGGSTKKLQETAMWLLSQQRDDGSFHDPCPVIHRDMQGGLVGSD" } ,
      annot {
        {
          data
            ftable {
              {
                data
                  prot {
                    name {
                      "complement component C4" } } ,
                partial TRUE ,
                location
                  int {
                    from 0 ,
                    to 128 ,
                    id
                      gi 1235 ,
                    fuzz-from
                      lim lt ,
                    fuzz-to
                      lim gt } } } } } } } ,
  annot {
    {
      data
        ftable {
          {
            data
              cdregion {
                frame two ,
                code {
                  id 1 } } ,
            partial TRUE ,
            product
              whole
                gi 1235 ,
            location
              mix {
                int {
                  from 0 ,
                  to 133 ,
                  id
                    gi 1234 ,
                  fuzz-from
                    lim lt } ,
                int {
                  from 298 ,
                  to 373 ,
                  id
                    gi 1234 } ,
                int {
                  from 530 ,
                  to 686 ,
                  id
                    gi 1234 } ,
                int {
                  from 924 ,
                  to 944 ,
                  id
                    gi 1234 ,
                  fuzz-to
                    lim gt } } ,
            dbxref {
              {
                db "GOA" ,
                tag
                  str "Q29410" } ,
              {
                db "HSSP" ,
                tag
                  str "1HZF" } ,
              {
                db "InterPro" ,
                tag
                  str "IPR008930" } ,
              {
                db "InterPro" ,
                tag
                  str "IPR011626" } ,
              {
                db "InterPro" ,
                tag
                  str "IPR019565" } ,
              {
                db "UniProtKB/TrEMBL" ,
                tag
                  str "Q29410" } } } } } } }
