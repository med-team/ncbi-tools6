Submit-block ::= {
  contact {
    contact {
      name name {
        last "Darwin",
        first "Charles",
        middle "",
        initials "",
        suffix "",
        title ""
      },
      affil std {
        affil "Oxbridge University",
        div "Evolutionary Biology Department",
        city "Camford",
        country "United Kingdom",
        street "1859 Tennis Court Lane",
        email "darwin@beagle.edu.uk",
        phone "01 44 171-007-1212",
        postal-code "OX1 2BH"
      }
    }
  },
  cit {
    authors {
      names std {
        {
          name name {
            last "Darwin",
            first "Charles",
            middle "",
            initials "C.R.",
            suffix "",
            title ""
          }
        }
      },
      affil std {
        affil "Oxbridge University",
        div "Evolutionary Biology Department",
        city "Camford",
        country "United Kingdom",
        street "1859 Tennis Court Lane",
        postal-code "OX1 2BH"
      }
    }
  },
  subtype new
}
Seqdesc ::= pub {
  pub {
    sub {
      authors {
        names std {
          {
            name ml "Darwin C",
          }
        },
        affil str "Evolutionary Biology Department, Oxbridge University,
 Camford, 1859 Tennis Court Lane, OX1 2BH, United Kingdom."
      },
      date std {
        year 2018,
        month 5,
        day 19
      },
    },
    article {
      title {
        name "In-depth analysis of one mysterious hypothetical protein of
 Saccharomyces cerevisiae."
      },
      authors {
        names std {
          {
            name ml "Darwin C",
            affil str "Evolutionary Biology Department, Oxbridge University,
 Camford, 1859 Tennis Court Lane, OX1 2BH, United Kingdom."
          },
        }
      },
      from journal {
        title {
          iso-jta "Nature",
          ml-jta "Nature",
          issn "1476-4687",
          name "Nature"
        },
        imp {
          date std {
            year 2018,
            month 5,
            day 19
          },
          language "eng",
          pubstatus aheadofprint,
          history {
            {
              pubstatus other,
              date std {
                year 2018,
                month 5,
                day 21,
                hour 6,
                minute 0
              }
            }
          }
        }
      },
      ids {
        pubmed 87654321,
        doi "10.0000/tpj.12345",
        other {
          db "ELocationID doi",
          tag str "10.0000/tpj.12345"
        }
      }
    }
  }
}
Seqdesc ::= user {
  type str "Submission",
  data {
    {
      label str "AdditionalComment",
      data str "ALT EMAIL:darwin@beagle.edu.uk"
    }
  }
}
Seqdesc ::= user {
  type str "Submission",
  data {
    {
      label str "AdditionalComment",
      data str "Submission Title:None"
    }
  }
}
